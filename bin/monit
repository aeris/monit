#!./bin/rails runner

Class.new Thor do
  def self.exit_on_failure?
    true
  end

  class Add < Thor
    class_option :name, type: :string, aliases: '-n', desc: 'check name'
    class_option :interval, type: :string, aliases: '-i', desc: 'check interval'

    desc 'ping <host>', 'create a ping check'

    def ping(host)
      name = options.fetch :name, host
      create! :ping, name, { host: host }
    end

    desc 'http <url>', 'create a HTTP check'
    method_option :code, type: :numeric, aliases: '-c', desc: 'expected HTTP code'

    def http(url)
      name = options.fetch :name, url
      code = options[:code]
      create! :http, name, { url: url, code: code }
    end

    desc 'tcp <host> <port>', 'create a TCP check'

    def tcp(host, port)
      name = options.fetch :name, "#{host}:#{port}"
      create! :tcp, name, { host: host, port: port }
    end

    desc 'udp <host> <port>', 'create a UDP check'

    def udp(host, port)
      name = options.fetch :name, "#{host}:#{port}"
      create! :udp, name, { host: host, port: port }
    end

    private

    def create!(type, name, config)
      interval = options[:interval]
      service  = Service.create! name: name, type: type, interval: interval, config: config.compact
      puts "Check #{service.id.colorize :green} created: #{service.config.ai}"
    end
  end

  desc 'add', 'create a check'
  subcommand 'add', Add

  desc 'ls', 'list checks'
  method_option :type, type: :string, aliases: '-t', desc: 'list only check with this type'
  method_option :name, type: :string, aliases: '-n', desc: 'list only check matching this name'

  def ls
    services = Service.all
    type     = options[:type]
    services = services.where type: type if type
    name     = options[:name]
    if name
      name     = name.gsub('?', '_').gsub('*', '%')
      services = services.where 'name like ?', name
    end

    services.each do |service|
      puts "#{service.id.colorize :blue} #{service.type} #{service.name}"
      puts service.config.ai
    end
  end

  desc 'rm <id>', 'delete a check'

  def rm(id)
    service = Service.find id
    service.destroy!
    puts "Check #{service.id.colorize :red} deleted"
  end

  desc 'check <id*>', 'perform a check'

  def check(*ids)
    services = Service
    services = ids.empty? ? services.all : services.where(id: ids)
    services.each &:check!
  end
end.start
