api_key = ENV['SENDGRID_API_KEY']
if api_key
  domain = ENV['SENDGRID_DOMAIN']
  Rails.application.configure do
    config.action_mailer.smtp_settings = {
      user_name:            :apikey,
      password:             api_key,
      domain:               domain,
      address:              'smtp.sendgrid.net',
      port:                 587,
      authentication:       :plain,
      enable_starttls_auto: true
    }
  end
end
