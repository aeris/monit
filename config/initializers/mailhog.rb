if Rails.env.development? && ENV['DISABLE_MAILHOG'].nil?
  Rails.application.configure do
    config.action_mailer.smtp_settings = {
      address: ENV.fetch('MAILHOG_HOST', 'localhost'),
      port:    ENV.fetch('MAILHOG_PORT', 1025).to_i
    }
  end
end
