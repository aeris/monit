HttpLog.configure do |config|
  config.enabled               = ENV['HTTP_LOG_ENABLED']
  config.log_headers           = true
  config.url_blacklist_pattern = /sentry/
end if defined? HttpLog
