ActiveSupport.on_load :active_record do
  ActiveRecord::Type.register :duration, ActiveSupport::Duration::Type
end
