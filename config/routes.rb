Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "application#main"
  resources :services
  resources :checks

  get '/sidebar', to: 'application#sidebar', as: 'sidebar'
  get '/dashboard', to: 'application#dashboard', as: 'dashboard'
  # get '/service/id', to: 'service#view', as: 'service'
end
