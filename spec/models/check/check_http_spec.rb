require 'rails_helper'

describe Check::Http do
  describe :check do
    it 'check on 80 port for HTTP request' do
      subject = Check::Http.new({ url: 'http://example.org/foo/bar' })
      allow(Addrinfo).to receive(:getaddrinfo)
                           .with('example.org', 80, nil, :STREAM)
                           .and_return [Addrinfo.tcp('127.0.0.1', 80),
                                        Addrinfo.tcp('::1', 80)]
      stub = stub_request(:get, 'http://example.org/foo/bar').to_return(body: 'ok')
      expect(subject.process).to eq({
                                      { host: 'example.org', port: 80, ip: '127.0.0.1' } => 200,
                                      { host: 'example.org', port: 80, ip: '::1' }       => 200
                                    })
      expect(subject.ok?).to be_truthy
      expect(stub).to have_been_requested.times(2)
    end

    it 'check on 443 port for HTTPS request' do
      subject = Check::Http.new({ url: 'https://example.org/foo/bar' })
      allow(Addrinfo).to receive(:getaddrinfo)
                           .with('example.org', 443, nil, :STREAM)
                           .and_return [Addrinfo.tcp('127.0.0.1', 443),
                                        Addrinfo.tcp('::1', 443)]
      stub = stub_request(:get, 'https://example.org/foo/bar').to_return(body: 'ok')
      expect(subject.process).to eq({
                                      { host: 'example.org', port: 443, ip: '127.0.0.1' } => 200,
                                      { host: 'example.org', port: 443, ip: '::1' }       => 200
                                    })
      expect(subject.ok?).to be_truthy
      expect(stub).to have_been_requested.times(2)
    end

    it 'allow custom status code' do
      subject = Check::Http.new({ url: 'https://example.org/foo/bar', code: 302 })
      allow(Addrinfo).to receive(:getaddrinfo)
                           .with('example.org', 443, nil, :STREAM)
                           .and_return [Addrinfo.tcp('::1', 443)]
      stub = stub_request(:get, 'https://example.org/foo/bar').to_return(status: 302)
      expect(subject.process).to eq({ { host: 'example.org', port: 443, ip: '::1' } => 302 })
      expect(subject.ok?).to be_truthy
      expect(stub).to have_been_requested.times(1)
    end

    it 'return DNS errors' do
      subject = Check::Http.new({ url: 'https://example.org/foo/bar' })
      allow(Addrinfo).to receive(:getaddrinfo)
                           .with('example.org', 443, nil, :STREAM)
                           .and_raise SocketError, 'getaddrinfo: Name or service not known'
      expect(subject.process).to eq({ { host: 'example.org', port: 443 } => { error: 'getaddrinfo: Name or service not known' } })
      expect(subject.ok?).to be_falsey
    end

    it 'return network errors' do
      subject = Check::Http.new({ url: 'https://example.org/foo/bar' })
      allow(Addrinfo).to receive(:getaddrinfo)
                           .with('example.org', 443, nil, :STREAM)
                           .and_return [Addrinfo.tcp('::1', 443)]
      stub = stub_request(:get, 'https://example.org/foo/bar').to_timeout
      expect(subject.process).to eq({ { host: 'example.org', port: 443, ip: '::1' } => { error: 'connection error: Connection timed out' } })
      expect(subject.ok?).to be_falsey
      expect(stub).to have_been_requested.times(1)
    end
  end
end
