require 'rails_helper'

describe Service do
  before do
    Service.delete_all
  end

  describe :check! do
    it 'must detect down change correctly and act accordingly' do
      service = Service.create! name: :test, type: :ping, config: { host: 'example.org' }
      check   = double Check::Ping, process: []
      allow(Check).to receive(:init).with(service.type, service.config)
                                    .and_return(check)

      now = Time.utc 2020, 01, 01, 00, 00, 00
      Timecop.freeze now do
        allow(check).to receive(:ok?).and_return(true).at_least(:once)
        expect { service.check! }.to not_change { ActionMailer::Base.deliveries.count } \
                                 .and not_change { Down.all.count }
      end

      Timecop.freeze from = (now += 5.minutes) do
        allow(check).to receive(:ok?).and_return(false).at_least(:once)
        expect { service.check! }.to change { ActionMailer::Base.deliveries.count }.by(1) \
                                   .and change { Down.all.count }.by(1)
        mail = ActionMailer::Base.deliveries.pop
        expect(mail.subject).to eq 'Service is DOWN: [ping] test'
        down = Down.first
        expect(down.from).to eq now
        expect(down.to).to be_nil
      end

      Timecop.freeze now += 5.minutes do
        expect { service.check! }.to not_change { ActionMailer::Base.deliveries.count } \
                                 .and not_change { Down.all.count }
      end

      Timecop.freeze now += 5.minutes do
        expect(check).to receive(:ok?).and_return(true).at_least(:once)
        expect { service.check! }.to change { ActionMailer::Base.deliveries.count }.by(1) \
                                   .and not_change { Down.all.count }
        mail = ActionMailer::Base.deliveries.pop
        expect(mail.subject).to eq 'Service is UP: [ping] test'
        down = Down.first
        expect(down.from).to eq from
        expect(down.to).to eq now
        expect(down.duration).to eq 10.minutes
      end

      Timecop.freeze now += 5.minutes do
        expect { service.check! }.to not_change { ActionMailer::Base.deliveries.count } \
                                 .and not_change { Down.all.count }
      end
    end
  end

  describe :uptime do
    let!(:service) { Service.create! name: :test, type: :ping, config: { host: 'example.org' } }
    let!(:check) { service.checks.create! created_at: '2020-01-01', result: true }

    it 'must exclude downtime prior to uptime period' do
      service.downs.create! from: '2019-01-01', to: '2020-01-01'
      Timecop.freeze 2021, 01, 01 do
        expect(service.uptime[1.year]).to eq 1
      end
    end

    it 'must only include the last downtime part of overlapping uptime period' do
      service.downs.create! from: '2019-07-01', to: '2020-07-01'
      Timecop.freeze 2021, 01, 01 do
        expect(service.uptime[1.year]).to eq 1-(182.0/366)
      end
    end

    it 'must include all the downtime later to uptime period' do
      service.downs.create! from: '2020-03-01', to: '2020-07-01'
      Timecop.freeze 2021, 01, 01 do
        expect(service.uptime[1.year]).to eq 1-(122.0/366)
      end
    end

    it 'must restrict uptime period if check created before' do
      service.downs.create! from: '2020-03-01', to: '2020-07-01'
      Timecop.freeze 2020, 06, 01 do
        expect(service.uptime[1.year]).to eq 1-(122.0/152)
      end
    end
  end
end
