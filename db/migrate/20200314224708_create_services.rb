class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services, id: :uuid do |t|
      t.string :name, null: false
      t.column :type, :service, null: false
      t.string :interval
      t.jsonb :config, null: false
      t.timestamps
    end
  end
end
