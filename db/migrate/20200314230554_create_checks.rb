class CreateChecks < ActiveRecord::Migration[5.2]
  def change
    create_table :checks, id: :uuid do |t|
      t.references :service, type: :uuid, null: false
      t.boolean :result, null: false
      t.timestamps
    end

    add_foreign_key :checks, :services
  end
end
