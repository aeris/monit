class CreateDowns < ActiveRecord::Migration[6.0]
  def change
    create_table :downs, id: :uuid do |t|
      t.references :service, type: :uuid, null: false
      t.datetime :from, null: false
      t.datetime :to
      t.timestamps
    end
  end
end
