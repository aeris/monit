class CreateResults < ActiveRecord::Migration[6.0]
  def change
    create_table :results, id: :uuid do |t|
      t.references :check, type: :uuid, null: false
      t.jsonb :key
      t.jsonb :value
      t.timestamps
    end

    add_foreign_key :results, :checks
  end
end
