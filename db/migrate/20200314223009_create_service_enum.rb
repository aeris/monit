class CreateServiceEnum < ActiveRecord::Migration[5.2]
  def up
    types = Service.types.values.collect { |t| "'#{t}'" }.join ', '
    execute <<-SQL
      CREATE TYPE service AS ENUM (#{types});
    SQL
  end

  def down
    execute <<-SQL
      DROP TYPE service;
    SQL
  end
end
