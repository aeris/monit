module Utils
  class Socket < ::Socket
    TIMEOUT      = 1.seconds.to_i.freeze
    TIMEOUT_SOCK = [TIMEOUT, 0].pack('l_2').freeze

    def initialize(addrinfo)
      @addrinfo = addrinfo
      @sockaddr = ::Socket.sockaddr_in @addrinfo.ip_port, @addrinfo.ip_address
      super @addrinfo.pfamily, @addrinfo.socktype, @addrinfo.protocol
      self.setsockopt ::Socket::SOL_SOCKET, ::Socket::SO_SNDTIMEO, TIMEOUT_SOCK
      self.setsockopt ::Socket::SOL_SOCKET, ::Socket::SO_RCVTIMEO, TIMEOUT_SOCK
    end

    def connect
      begin
        self.connect_nonblock @sockaddr
        block_given? ? yield(self) : nil
      rescue ::IO::WaitReadable
        raise unless IO.select [self], nil, nil, TIMEOUT
        retry
      rescue ::IO::WaitWritable
        raise unless IO.select nil, [self], nil, TIMEOUT
        retry
      ensure
        self.close
      end
    end

    def send(message)
      begin
        self.sendmsg_nonblock message
      rescue ::IO::WaitWritable
        raise unless IO.select nil, [self], nil, TIMEOUT
        retry
      end
    end

    def recv(message)
      begin
        self.recvmsg_nonblock message
      rescue ::IO::WaitReadable
        raise unless IO.select [self], nil, nil, TIMEOUT
        retry
      end
    end

    def self.tcp(addrinfo)
      self.new(addrinfo).connect
    end

    def self.udp(addrinfo)
      self.new(addrinfo).connect do |sock|
        sock.send ''
        sock.send "\0" * 64
        sock.recv 1
      end
    end
  end
end
