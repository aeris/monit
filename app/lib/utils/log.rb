module Utils
  module Log
    def self.log(level, msg)
      level = Logger::Severity.const_get level.to_s.upcase
      unless block_given?
        $stdout.puts msg
        return
      end

      begin
        result = yield
        suffix = result ? ", #{result}" : ''
        $stdout.puts "#{msg}: #{'OK'.colorize :green}#{suffix}"
        result
      rescue => e
        $stderr.puts "#{msg}: #{'KO'.colorize :red}, #{e}"
        raise
      end
    end
  end
end
