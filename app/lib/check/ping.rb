class Check::Ping < Check::Base
  def check
    host = @config.fetch :host
    Utils::Log.log :info, "Testing #{'ping'.colorize :yellow} connection on #{host.colorize :yellow}"
    Addrinfo.getaddrinfo(host, nil, nil, :RAW)
      .collect do |addrinfo|
      ip   = addrinfo.ip_address
      key  = { host: host, ip: ip }
      ping = Utils::Log.log :info, "  #{ip.colorize :yellow}" do
        Net::Ping::External.new(ip).ping
      end
      [key, ping]
    rescue => e
      [key, { error: e.message }]
    end.to_h
  end

  def ok?
    @values.all? { |_, ping| ping === true }
  end
end
