class Check::Base
  attr_reader :values

  def initialize(config)
    @config = config
  end

  def process
    @values ||= self.check
  end
end
