class Check::Udp < Check::Base
  def check
    host = @config.fetch :host
    port = @config.fetch :port

    Utils::Log.log :info, "Testing #{'UDP'.colorize :yellow} connection for #{"#{host}:#{port}".colorize :yellow}"
    Addrinfo.getaddrinfo(host, port, nil, :DGRAM)
      .collect do |addrinfo|
      ip  = addrinfo.ip_address
      key = { host: host, port: port, ip: ip }
      Utils::Log.log :info, "  #{ip.colorize :yellow}" do
        Utils::Socket.udp addrinfo
      end
      [key, true]
    rescue => e
      [key, { error: e.message }]
    end.to_h
  end

  def ok?
    @values.all? { |_, v| v === true }
  end
end
