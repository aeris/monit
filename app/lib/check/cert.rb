class Check::Cert < Check::Base
  DEFAULT_CERT_DURATION = 1.month.freeze

  def check
    host = @config.fetch :host
    port = @config.fetch :port

    context = OpenSSL::SSL::SSLContext.new
    Addrinfo.getaddrinfo(host, port, nil, :STREAM)
      .collect do |addrinfo|
      ip         = addrinfo.ip_address
      key        = [host, port, ip]
      tcp_socket = TCPSocket.new ip, addrinfo.ip_port
      begin
        tls_client = OpenSSL::SSL::SSLSocket.new tcp_socket, context
        begin
          tls_client.hostname = host
          tls_client.connect
          cert      = tls_client.peer_cert
          not_after = cert.not_after
          [key, not_after]
        ensure
          tls_client.close
        end
      ensure
        tcp_socket.close
      end
    rescue => e
      [key, { error: e.message }]
    end.to_h
  end

  def ok?
    duration = @config[:duration]
    duration &&= ActiveSupport::Duration.parse duration
    duration ||= DEFAULT_CERT_DURATION
    @values.all? { |_, not_after| not_after - Time.now >= duration }
  end
end
