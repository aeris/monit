class Check::Http < Check::Base
  def check
    url  = @config.fetch :url
    url  = URI.parse url
    host = url.host
    port = url.port

    client = HTTP::Client.new
    Utils::Log.log :info, "Testing #{'HTTP'.colorize :yellow} connection on #{"#{host}:#{port}".colorize :yellow}"
    Addrinfo.getaddrinfo(host, port, nil, :STREAM)
      .collect do |addrinfo|
      ip   = addrinfo.ip_address
      key  = { host: host, port: port, ip: ip }
      code = Utils::Log.log :info, "  #{ip.colorize :yellow}" do
        req = HTTP.build_request :get, url
        req.define_singleton_method(:socket_host) { ip }
        response = client.perform req, client.default_options
        response.code
      end
      [key, code]
    rescue => e
      [key, { error: e.message }]
    end.to_h
  rescue => e
    { { host: host, port: port } => { error: e.message } }
  end

  def ok?
    expected_code = @config.fetch :code, 200
    @values.all? { |_, code| code === expected_code }
  end
end
