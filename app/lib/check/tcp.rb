class Check::Tcp < Check::Base
  def check
    host = @config.fetch :host
    port = @config.fetch :port

    Utils::Log.log :info, "Testing #{'TCP'.colorize :yellow} connection for #{"#{host}:#{port}".colorize :yellow}"
    Addrinfo.getaddrinfo(host, port, nil, :STREAM)
      .collect do |addrinfo|
      ip  = addrinfo.ip_address
      key = { host: host, port: port, ip: ip }
      Utils::Log.log :info, "  #{ip.colorize :yellow}" do
        Utils::Socket.tcp addrinfo
      end
      [key, true]
    rescue => e
      [key, { error: e.message }]
    end.to_h
  end

  def ok?
    @values.all? { |_, v| v === true }
  end
end
