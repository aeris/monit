class NotifierMailer < ApplicationMailer
  default to:       ENV.fetch('EMAIL_TO'),
          from:     ENV.fetch('EMAIL_FROM'),
          reply_to: ENV.fetch('EMAIL_REPLY_TO')

  def service_fail
    @service = params.fetch :service
    @down    = params.fetch :down
    self.mail subject: "Service is DOWN: [#{@service.type}] #{@service.name}"
  end

  def service_recover
    @service = params.fetch :service
    @down    = params.fetch :down
    self.mail subject: "Service is UP: [#{@service.type}] #{@service.name}"
  end
end
