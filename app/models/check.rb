class Check < ApplicationRecord
  belongs_to :service
  has_many :results, dependent: :destroy

  def self.init(type, config)
    clazz = self.const_get type.to_s.camelize
    clazz.new config.with_indifferent_access
  end
end
