class Service < ApplicationRecord
  self.inheritance_column = :_type
  enum type: %i[ping tcp udp http cert].collect { |k| [k, k.to_s] }.to_h.freeze
  attribute :interval, :duration
  validates :type, inclusion: { in: self.types.keys }
  has_many :checks, dependent: :destroy
  has_many :downs, dependent: :destroy

  def check!
    last     = self.last_check
    interval = self.interval
    if interval
      delay = Time.now - last.created_at
      return if delay < interval
    end

    check  = Check.init self.type, self.config
    values = check.process
    result = check.ok?

    check = self.checks.create! result: result
    values.each { |k, v| check.results.create! key: k, value: v }

    if last
      case [last.result, result]
      when [true, false]
        down = self.downs.create! from: check.created_at
        NotifierMailer.with(service: self, down: down).service_fail.deliver_now
      when [false, true]
        down = self.last_down
        down.update! to: check.created_at if down
        NotifierMailer.with(service: self, down: down).service_recover.deliver_now
      end
    end
  end

  UPTIME_DURATIONS = [1.year, 1.month, 1.week, 1.day].freeze

  def uptime
    first_check = self.checks.order(created_at: :asc).limit(1).first
    return nil unless first_check

    now    = Time.now.utc
    uptime = UPTIME_DURATIONS.map { |d| [d, 0.second] }.to_h
    ago    = UPTIME_DURATIONS.map { |d| [d, d.ago(now).utc] }.to_h
    start  = ago.fetch UPTIME_DURATIONS.first

    self.downs.where('"to" >= ?', start).each do |down|
      UPTIME_DURATIONS.each do |duration|
        start            = ago[duration]
        uptime[duration] += down.to.utc - [start, down.from.utc].max if start <= down.to
      end
    end

    check_start = first_check.created_at.utc
    uptime.map do |duration, downtime|
      start = ago[duration]
      max   = [check_start, start].max
      diff  = (now - max).seconds
      percentage = 1 - downtime / diff
      [duration, percentage]
    end.to_h
  end

  def last_check
    self.checks.order(created_at: :desc).limit(1).first
  end

  def last_down
    self.downs.order(created_at: :desc).limit(1).first
  end
end
