class Down < ApplicationRecord
  belongs_to :service

  def duration
    to = self.to
    return nil unless to
    to - self.from
  end
end
