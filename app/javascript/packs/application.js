require("jquery")
require("vue")
require("bootstrap")
require('@fortawesome/fontawesome-free')

import 'css/application'
import 'bootstrap'

import Routes from '../routes/index.js.erb';
window.Routes = Routes;

import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import 'axios'

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    router,
    render: h => h(App)
  }).$mount()
  document.body.appendChild(app.$el)
})