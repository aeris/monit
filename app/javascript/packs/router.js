import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/Login.vue'
import Dashboard from '../views/Dashboard.vue'
import Service from '../views/Service.vue'
import NewService from '../views/NewService.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Dashboard
        },
        {
            path: '/service',
            name: 'service',
            component: Service
        },
        {
          path: '/service/new',
          name: 'newservice',
          component: NewService
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        }
    ]
})