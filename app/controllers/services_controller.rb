class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]

  def show
    render json: @service
  end

  def new
    @service = Service.new
  end

  CONFIG_PARAMS = {
      ping: %i[host],
      http: %i[url code],
      tcp: %i[host port],
      udp: %i[host port]
  }

  def create
    type = params[:service][:type].to_sym
    config = params[:service].slice *CONFIG_PARAMS[type]
    p = { name: params[:service][:name], type: type, config: config }

    @service = Service.create! **p
    redirect_to @service, notice: t('notice.service.created', service: @service.name)
  end

  def edit
  end

  def update
    type = params[:service][:type].to_sym
    config = params[:service].slice *CONFIG_PARAMS[type]
    p = { id: params[:id], name: params[:service][:name], type: type, config: config }

    if @service.update(p)
      redirect_to @service, notice: t('notice.service.updated', service: @service.name)
    else
      render :edit
    end
  end

  private
  def set_service
    @service = Service.find(params[:id])
  end

  def service_params
    params.require(:service).permit(
      :id,
      :name,
      :type,
      :config
    )
  end
end
