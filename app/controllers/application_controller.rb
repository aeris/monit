class ApplicationController < ActionController::Base

  def main
    @services = Service.all
    @checks = Check.all
  end

  def sidebar
    @services = Service.all
    sidebar = []
    percent = 100
    @services.each do |s|
      ref = nil
      unless s.config[:http].nil?
        ref = s.config[:http]
      end
      sidebar.push(
          {
              percent: percent,
              type: s.type,
              ref: ref,
              name: s.name,
              error: nil
          })
      percent -= 10
    end
    render json: sidebar
  end

  def dashboard
    @services = Service.all
    @checks = Check.all
    dashboard = {
        total_services: @services.size,
        total_checks: @checks.size,
        stats: @services
    }
    render json: dashboard
  end

end
